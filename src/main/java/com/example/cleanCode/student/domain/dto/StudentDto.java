package com.example.cleanCode.student.domain.dto;

import lombok.Data;

@Data
public class StudentDto {
    private String name;
    private String email;
    private GenderDto gender;
}