package com.example.cleanCode.student.service.service;

import com.example.cleanCode.student.infra.dao.StudentRepository;
import com.example.cleanCode.student.service.exception.BadRequestException;
import com.example.cleanCode.student.service.exception.StudentNotFoundException;
import com.example.cleanCode.student.domain.model.Student;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public void addStudent(Student student) {
        FactoryGender factory = new FactoryGender();
        factory.getGender(student.getGender());
        Boolean existsEmail = studentRepository
                .selectExistsEmail(student.getEmail());
        if (existsEmail) {
            throw new BadRequestException(
                    "Email " + student.getEmail() + " taken");
        }

        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        if(!studentRepository.existsById(studentId)) {
            throw new StudentNotFoundException(
                    "Student with id " + studentId + " does not exists");
        }
        studentRepository.deleteById(studentId);
    }
}
