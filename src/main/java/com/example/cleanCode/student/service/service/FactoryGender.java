package com.example.cleanCode.student.service.service;

public class FactoryGender {

    public GenderDefinition getGender(Gender gender) {
        if (gender.equals(Gender.M))
            return new Male(gender);
        if (gender.equals(Gender.F))
            return new Female(gender);
        return null;
    }
}
