package com.example.cleanCode.student.service.service;

import com.example.cleanCode.student.domain.model.Student;

public abstract class GenderDefinition {

    public Gender gender = new Student().getGender();

}
class Male extends GenderDefinition {

    public Male(Gender gender) {
        this.gender = gender;
        System.out.println("The gender is Male");
    }
}

class Female extends GenderDefinition {

    public Female(Gender gender) {
        this.gender = gender;
        System.out.println("The gender is Female");
    }
}


