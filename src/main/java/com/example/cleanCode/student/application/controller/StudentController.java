package com.example.cleanCode.student.application.controller;

import com.example.cleanCode.student.domain.dto.StudentDto;
import com.example.cleanCode.student.domain.model.Student;
import com.example.cleanCode.student.service.service.StudentService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/students")
@AllArgsConstructor
public class StudentController {
    @Autowired
    private ModelMapper modelMapper;

    private final StudentService studentService;

    @GetMapping
    public List<StudentDto> getAllStudents() {
        return studentService.getAllStudents().stream().map(post -> modelMapper.map(post,StudentDto.class)).collect(Collectors.toList());
    }

    @PostMapping
    public void addStudent(@Valid @RequestBody Student student) {
//        FactoryGender factory = new FactoryGender();
//        factory.getGender(student.getGender());
        studentService.addStudent(student);
    }


    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(
            @PathVariable("studentId") Long studentId) {
        studentService.deleteStudent(studentId);
    }
}
